package android.animemovierecommender.apkia.com.animemovierecommender.Activity;

import android.animemovierecommender.apkia.com.animemovierecommender.Model.Movie;
import android.animemovierecommender.apkia.com.animemovierecommender.R;
import android.animemovierecommender.apkia.com.animemovierecommender.helper.ReferenceMap;
import android.animemovierecommender.apkia.com.animemovierecommender.helper.UtilityFunctions;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.exit;

public class DetailActivity extends AppCompatActivity {

    private Menu menu;

    ImageView poster;
    TextView title;
    TextView year;
    TextView genre;
    TextView language;
    TextView vote;
    TextView originalTitle;
    TextView overview;
    private Movie[] mMovie;
    String genres="";
    String posterPath=null;
    String finalGenre="";
    String voteRate="";
    boolean check;
    private ArrayList<Movie> arrlistofWatchMovies;
    private String movieTitle;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        android.support.v7.app.ActionBar actionbar;
        actionbar=getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("");


        boolean is_tablet=getResources().getBoolean(R.bool.is_tablet);




        title= (TextView) findViewById(R.id.textView_title);
        year= (TextView) findViewById(R.id.textView_year);
        genre= (TextView) findViewById(R.id.textView_genre);
        language= (TextView) findViewById(R.id.textView_language);
        vote= (TextView) findViewById(R.id.textView_vote);
        poster= (ImageView) findViewById(R.id.imageView_poster);
        overview= (TextView) findViewById(R.id.textView_overview);


        mContext=this;

        float density = mContext.getResources().getDisplayMetrics().density;

        ReferenceMap.ReferenceList refList = ReferenceMap.getInstance().getReferenceList(this);
        if(refList != null) {
            Movie mMovie = (Movie) refList.get("myKey");
            title.setText(mMovie.getTitle());

            movieTitle=mMovie.getTitle();
            year.setText(mMovie.getReleaseDate()+"");
           language.setText(mMovie.getLanguage());
            char star='\u2605';
            voteRate=mMovie.getVote();
            vote.setText(star+" " +voteRate);
            overview.setText(mMovie.getOverview());


            if (is_tablet) {
                if (mMovie.getPoster() != null) {
                    Picasso.with(this).load("https://image.tmdb.org/t/p/w500" + mMovie.getPoster()).into(poster);
                    posterPath = mMovie.getPoster();
                } else {
                    Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").into(poster);
                }
            }else {
                while (true) {
                    if (density >= 4.0) {
                        if (mMovie.getPoster() != null) {
                            Picasso.with(this).load("https://image.tmdb.org/t/p/w500" + mMovie.getPoster()).into(poster);
                            posterPath = mMovie.getPoster();
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(500, 700).into(poster);
                        }
                        break;
                    } else if (density >= 3.0) {
                        if (mMovie.getPoster() != null) {
                            Picasso.with(this).load("https://image.tmdb.org/t/p/w500" + mMovie.getPoster()).into(poster);
                            posterPath = mMovie.getPoster();
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(500, 700).into(poster);
                        }
                        break;
                    } else if (density >= 2.0) {
                        if (mMovie.getPoster() != null) {
                            Picasso.with(this).load("https://image.tmdb.org/t/p/w300" + mMovie.getPoster()).into(poster);
                            posterPath = mMovie.getPoster();
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(400, 400).into(poster);
                        }
                        break;
                    } else if (density >= 1.5) {
                        if (mMovie.getPoster() != null) {
                            Picasso.with(this).load("https://image.tmdb.org/t/p/w300" + mMovie.getPoster()).resize(300,300).centerInside().into(poster);
                            posterPath = mMovie.getPoster();
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(400, 400).into(poster);
                        }
                        break;
                    } else if (density >= 1.0) {
                        if (mMovie.getPoster() != null) {
                            Picasso.with(this).load("https://image.tmdb.org/t/p/w300" + mMovie.getPoster()).resize(300,300).centerInside().into(poster);
                            posterPath = mMovie.getPoster();
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(400, 400).into(poster);
                        }
                        break;
                    }
                }

            }





            genres=mMovie.getGenres().toString().replaceAll("\\[|\\]|\\s","");
            finalGenre = UtilityFunctions.getGenreString(genres);
            genre.setText(finalGenre+"");

        }








    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.result, menu);
        this.menu = menu;

        List<Movie> listAll;
        long count = Movie.count(Movie.class);
        if(count>0) {
            listAll = (ArrayList<Movie>) Movie.listAll(Movie.class);
            for (Movie temp : listAll) {
                if (temp.getTitle().contains(movieTitle)) {
                    menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_grade_black_24dp));
                    break;
                } else {

                }

            }
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                // NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        if (id == R.id.action_watch) {



            Movie movie=new Movie();
            movie.setPoster(posterPath);
            movie.setTitle(title.getText().toString());
            movie.setLanguage(language.getText().toString());
            movie.setVote(voteRate);
            movie.setStringGenre(finalGenre);
            movie.setStringYear(year.getText().toString());

            List<Movie> listAll;
            long count = Movie.count(Movie.class);
            if(count>0) {
                listAll = (ArrayList<Movie>) Movie.listAll(Movie.class);
                for (Movie temp : listAll) {
                    if (temp.getTitle().contains(movieTitle)) {
                        check = true;
                        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_grade_black_24dp));
                        break;
                    } else {
                        check = false;
                        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_grade_white_24dp));
                    }

                }
            }

            if (check==false) {
                movie.save();
                menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_grade_black_24dp));
            }else{
                UtilityFunctions.alertAlreadyInDatabase(mContext);
            }

        }
        return super.onOptionsItemSelected(item);
    }





}
