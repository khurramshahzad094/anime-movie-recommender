package android.animemovierecommender.apkia.com.animemovierecommender.Rest;

import android.animemovierecommender.apkia.com.animemovierecommender.Model.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by khurram on 11/04/2017.
 */

public interface ApiInterface {

    @GET("movie/")
    Call<MovieResponse> getMovieDetails(@Query("api_key") String apiKey,
                                         @Query("certification") String certification,
                                         @Query("primary_release_date.gte") String fromYear,
                                         @Query("primary_release_date.lte") String toYear,
                                         @Query("with_genres") String genre,
                                        @Query("sort_by") String sort);



    @GET("tv/")
    Call<MovieResponse> getTVDetails(@Query("api_key") String apiKey,
                                        @Query("certification") String certification,
                                        @Query("first_air_date.gte") String fromYear,
                                        @Query("first_air_date.lte") String toYear,
                                        @Query("with_genres") String genre,
                                        @Query("sort_by") String sort);



}
