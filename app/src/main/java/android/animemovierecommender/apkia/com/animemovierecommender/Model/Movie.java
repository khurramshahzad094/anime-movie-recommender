package android.animemovierecommender.apkia.com.animemovierecommender.Model;

import android.animemovierecommender.apkia.com.animemovierecommender.helper.UtilityFunctions;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by khurram on 23/04/2017.
 */

public class Movie extends SugarRecord {


    @SerializedName("poster_path")
    private String poster;

    @SerializedName("vote_average")
    private String vote;

    @SerializedName("overview")
    private String overview;

    @SerializedName(value = "release_date",alternate={"first_air_date"})
    private String releaseDate;

    @SerializedName("original_language")
    private String language;


    @SerializedName(value = "title",alternate={"name"})
    private String title;

   @SerializedName("genre_ids")
    private ArrayList<Integer> mGenres=new ArrayList<Integer>();

    @SerializedName("original_title")
    private String alternativeTitle;

    private String StringGenre;

    private String StringYear;


    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        String year="";
        try {
             year=UtilityFunctions.parseDate(releaseDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return year;

    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getLanguage() {
        return UtilityFunctions.languageChooser(language);
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Integer> getGenres() {
        return mGenres;
    }

    public void setGenres(ArrayList<Integer> genres) {
        mGenres = genres;
    }

    public String getAlternativeTitle() {
        return alternativeTitle;
    }

    public void setAlternativeTitle(String alternativeTitle) {
        this.alternativeTitle = alternativeTitle;
    }

    public String getStringGenre() {
        return StringGenre;
    }

    public void setStringGenre(String StringGenre) {
        this.StringGenre = StringGenre;
    }

    public String getStringYear() {
        return StringYear;
    }

    public void setStringYear(String stringYear) {
        StringYear = stringYear;
    }
}
