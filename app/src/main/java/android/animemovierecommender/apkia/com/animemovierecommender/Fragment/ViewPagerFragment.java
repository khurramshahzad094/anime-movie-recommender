package android.animemovierecommender.apkia.com.animemovierecommender.Fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.animemovierecommender.apkia.com.animemovierecommender.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPagerFragment extends Fragment {


    public ViewPagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_view_pager, container, false);

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            SearchFragment searchFragment =new SearchFragment();
            WatchListFragment watchListFragment =new WatchListFragment();

            @Override
            public Fragment getItem(int position) {
                return position ==0 ? searchFragment : watchListFragment;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return position ==0 ? "Search" : "To Watch";

            }

            @Override
            public int getCount() {
                return 2;
            }
        });


        TabLayout tabLayout= (TabLayout) view.findViewById(R.id.TabLayout);
        tabLayout.setTabTextColors(android.content.res.ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }



}
