package android.animemovierecommender.apkia.com.animemovierecommender.Fragment;


import android.animemovierecommender.apkia.com.animemovierecommender.Adapter.WatchListAdapter;
import android.animemovierecommender.apkia.com.animemovierecommender.Model.Movie;
import android.animemovierecommender.apkia.com.animemovierecommender.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class WatchListFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private Context mContext;
    WatchListAdapter mAdapter;
    ArrayList<Movie> arrlistofWatchMovies;
    String movieTitle;

    TextView mEmptyTextView;
    private DividerItemDecoration mDividerItemDecoration;

    public WatchListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_watch_list, container, false);


        mContext=getActivity();

        mRecyclerView= (RecyclerView) view.findViewById(R.id.recyclerView);
        mEmptyTextView = (TextView) view.findViewById(R.id.textView_emptyList);

        mEmptyTextView.setVisibility(View.INVISIBLE);

        final List<Movie> movies = Movie.findWithQuery(Movie.class, "Select * from Movie");
        arrlistofWatchMovies = new ArrayList<Movie>(movies);


        if (arrlistofWatchMovies.isEmpty()){
            mEmptyTextView.setVisibility(View.VISIBLE);
        }

        mAdapter=new WatchListAdapter(arrlistofWatchMovies,mContext);
        mRecyclerView.setAdapter(mAdapter);
      //  mAdapter.notifyDataSetChanged();


        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);

        mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        mDividerItemDecoration.setDrawable(mContext.getResources().getDrawable(R.drawable.dividercolor));

        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        mAdapter.SetOnItemClickListener(new WatchListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                movieTitle=arrlistofWatchMovies.get(position).getTitle();
                long id=arrlistofWatchMovies.get(position).getId();
               // arrlistofWatchMovies.remove(position);
                arrlistofWatchMovies.remove(position);
                mAdapter.notifyDataSetChanged();

                Movie movie=Movie.findById(Movie.class,id);
                if (movie!=null) {
                    movie.delete();
                }
            }
        });

        return view;
    }

}
