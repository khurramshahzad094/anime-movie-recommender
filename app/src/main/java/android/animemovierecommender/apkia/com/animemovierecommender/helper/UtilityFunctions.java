package android.animemovierecommender.apkia.com.animemovierecommender.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by khurram on 25/04/2017.
 */

public class UtilityFunctions {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static void alertUserAboutError(Context context) {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setTitle("Network Error");
        b.setMessage("Connection is offline.");
        b.setIcon(android.R.drawable.ic_dialog_alert);
        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        b.create().show();

    }

    public static void alertAlreadyInDatabase(Context context) {
        AlertDialog.Builder b = new AlertDialog.Builder(context);
        b.setTitle("Alert");
        b.setMessage("Already in watch list.");
        b.setIcon(android.R.drawable.ic_dialog_alert);
        b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        b.create().show();

    }

    public static String parseDate(String s) throws ParseException {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(s);
        String newstring = new SimpleDateFormat("yyyy").format(date);
        return newstring;
    }

    public static String getGenreString(String genres) {
        List<String> genreList = Arrays.asList(genres.split(","));
        String finalGenre="";
        for(int i=0;i<genreList.size();i++){
            if(genreList.get(i).equals("28")){
                finalGenre+="Action,";
            }
            if(genreList.get(i).equals("12")){
                finalGenre+="Adventure,";
            } if(genreList.get(i).equals("16")){
                finalGenre+="Animation,";
            } if(genreList.get(i).equals("35")){
                finalGenre+="Comedy,";
            } if(genreList.get(i).equals("80")){
                finalGenre+="Crime,";
            } if(genreList.get(i).equals("99")){
                finalGenre+="Documentary,";
            } if(genreList.get(i).equals("18")){
                finalGenre+="drama,";
            } if(genreList.get(i).equals("10751")){
                finalGenre+="Family,";
            } if(genreList.get(i).equals("14")){
                finalGenre+="Fantasy,";
            } if(genreList.get(i).equals("36")){
                finalGenre+="History,";
            } if(genreList.get(i).equals("27")){
                finalGenre+="Horror,";
            } if(genreList.get(i).equals("10402")){
                finalGenre+="Music,";
            } if(genreList.get(i).equals("9648")){
                finalGenre+="Mystery,";
            } if(genreList.get(i).equals("10749")){
                finalGenre+="Romance,";
            } if(genreList.get(i).equals("878")){
                finalGenre+="Science Fiction,";
            } if(genreList.get(i).equals("10770")){
                finalGenre+="TV Movie,";
            } if(genreList.get(i).equals("53")){
                finalGenre+="Thriller,";
            } if(genreList.get(i).equals("24")){
                finalGenre+="drama,";
            } if(genreList.get(i).equals("10752")){
                finalGenre+="War,";
            } if(genreList.get(i).equals("37")){
                finalGenre+="Western,";
            }
        }
        return finalGenre;
    }

    public static String genreChooser(String genre) {

        if (genre.equals("Any")){
            genre="Any";
        }else if (genre.equals("Action")){
            genre="28";
        }else if (genre.equals("Adventure")){
            genre="12";
        }else if (genre.equals("Animation")){
            genre="16";
        }else if (genre.equals("Comedy")){
            genre="35";
        }else if (genre.equals("Crime")){
            genre="80";
        }else if (genre.equals("Documentary")){
            genre="99";
        }else if (genre.equals("Drama")){
            genre="18";
        }else if (genre.equals("Family")){
            genre="10751";
        }else if (genre.equals("Fantasy")){
            genre="14";
        }else if (genre.equals("History")){
            genre="36";
        }else if (genre.equals("Horror")){
            genre="27";
        }else if (genre.equals("Music")){
            genre="10402";
        }else if (genre.equals("Mystery")){
            genre="9648";
        }else if (genre.equals("Romance")){
            genre="10749";
        }else if (genre.equals("Science Fiction")){
            genre="878";
        }else if (genre.equals("TV Movie")){
            genre="10770";
        }else if (genre.equals("Thriller")){
            genre="53";
        }else if (genre.equals("War")){
            genre="10752";
        }else if (genre.equals("Western")){
            genre="37";
        }else {
            genre=null;
        }


        return  genre;
    }

    public static String languageChooser(String lang){
        if (lang.equals("en")){
            return "English";
        }else if (lang.equals("fr")){
            return "French";
        }else if (lang.equals("it")){
            return "Italian";
        }else if (lang.equals("de")){
            return "Deutsch";
        }else if (lang.equals("es")){
            return "Spanish";
        }else if (lang.equals("hi")){
            return "Hindi";
        }else{
            return lang;
        }
    }

}
