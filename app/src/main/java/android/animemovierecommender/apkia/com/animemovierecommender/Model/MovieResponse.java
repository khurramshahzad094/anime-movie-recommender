package android.animemovierecommender.apkia.com.animemovierecommender.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by khurram on 23/04/2017.
 */

public class MovieResponse {

    @SerializedName("page")
    private String page;

    @SerializedName("results")
    private ArrayList<Movie> mMovies;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public ArrayList<Movie> getMovies() {
        return mMovies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        mMovies = movies;
    }
}
