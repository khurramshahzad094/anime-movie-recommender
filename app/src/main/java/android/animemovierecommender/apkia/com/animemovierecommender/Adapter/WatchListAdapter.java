package android.animemovierecommender.apkia.com.animemovierecommender.Adapter;

import android.animemovierecommender.apkia.com.animemovierecommender.Model.Movie;
import android.animemovierecommender.apkia.com.animemovierecommender.R;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by khurram on 09/01/2017.
 */

public class WatchListAdapter extends RecyclerView.Adapter<WatchListAdapter.MovieViewHolder> {

    /*
    * since we are using a list view for DailyPrayer, so to implement the list we have to use an Adapter
    * as it adapts the view on the list and handles the position of each item on the list.
    * */

    private Context mContext;
    private ArrayList<Movie> mMovies;
    private OnItemClickListener mItemClickListener;


    public WatchListAdapter(ArrayList<Movie> movies, Context context){
        mMovies=movies;
        mContext=context;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_watch_list_item,parent,false);
        MovieViewHolder movieViewHolder=new MovieViewHolder(view);
        return  movieViewHolder;
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bindMovie(mMovies.get(position));
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }


    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView poster;
        TextView title;
        TextView year;
        TextView genre;
        TextView language;
        TextView vote;
        ImageButton delete;
        boolean is_tablet=mContext.getResources().getBoolean(R.bool.is_tablet);
        String genres="";


        public MovieViewHolder(View itemView) {
            super(itemView);
            title= (TextView) itemView.findViewById(R.id.textView_title);
            year= (TextView) itemView.findViewById(R.id.textView_year);
            genre= (TextView) itemView.findViewById(R.id.textView_genre);
            language= (TextView) itemView.findViewById(R.id.textView_language);
            vote= (TextView) itemView.findViewById(R.id.textView_vote);
            poster= (ImageView) itemView.findViewById(R.id.imageView_poster);
            delete= (ImageButton) itemView.findViewById(R.id.button_delete);

            delete.setOnClickListener(this);

        }

        public void bindMovie(final Movie movie){
            title.setText(movie.getTitle());
            year.setText(movie.getStringYear());
            language.setText(movie.getLanguage());
            vote.setText(movie.getVote());
            float density = mContext.getResources().getDisplayMetrics().density;
            if (is_tablet){
                if (movie.getPoster()!=null) {
                    Picasso.with(mContext).load("https://image.tmdb.org/t/p/w300" + movie.getPoster()).into(poster);
                }else {
                    Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").into(poster);
                }
            }else {
                while (true) {
                    if (density >= 4.0) {
                        if (movie.getPoster() != null) {
                            Picasso.with(mContext).load("https://image.tmdb.org/t/p/w500" + movie.getPoster()).into(poster);
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(400, 400).into(poster);
                        }
                        break;
                    } else if (density >= 3.0) {
                        if (movie.getPoster() != null) {
                            Picasso.with(mContext).load("https://image.tmdb.org/t/p/w300" + movie.getPoster()).into(poster);
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(400, 400).into(poster);
                        }
                        break;
                    } else if (density >= 2.0) {
                        if (movie.getPoster() != null) {
                            Picasso.with(mContext).load("https://image.tmdb.org/t/p/w300" + movie.getPoster()).resize(300,300).centerInside().into(poster);
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(300, 300).into(poster);
                        }
                        break;
                    } else if (density >= 1.5) {
                        if (movie.getPoster() != null) {
                            Picasso.with(mContext).load("https://image.tmdb.org/t/p/w300" + movie.getPoster()).resize(250,250).centerInside().into(poster);
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(250, 250).into(poster);
                        }
                        break;
                    } else if (density >= 1.0) {
                        if (movie.getPoster() != null) {
                            Picasso.with(mContext).load("https://image.tmdb.org/t/p/w300" + movie.getPoster()).resize(240,240).centerInside().into(poster);
                        } else {
                            Picasso.with(mContext).load("https://www.imdb.com/images/nopicture/large/film.png").centerInside()
                                    .resize(240, 240).into(poster);
                        }
                        break;
                    }
                }

            }
            genre.setText(movie.getStringGenre()+"");


        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener!=null)
                mItemClickListener.onItemClick(v, getPosition());
        }



    }



    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


}
