package android.animemovierecommender.apkia.com.animemovierecommender.Activity;

import android.animemovierecommender.apkia.com.animemovierecommender.Adapter.MovieAdapter;
import android.animemovierecommender.apkia.com.animemovierecommender.Model.Movie;
import android.animemovierecommender.apkia.com.animemovierecommender.Model.MovieResponse;
import android.animemovierecommender.apkia.com.animemovierecommender.R;
import android.animemovierecommender.apkia.com.animemovierecommender.Rest.ApiClient;
import android.animemovierecommender.apkia.com.animemovierecommender.Rest.ApiInterface;
import android.animemovierecommender.apkia.com.animemovierecommender.helper.ReferenceMap;
import android.animemovierecommender.apkia.com.animemovierecommender.helper.UtilityFunctions;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.animemovierecommender.apkia.com.animemovierecommender.R.id.recyclerView;

public class ResultActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = ResultActivity.class.getSimpleName();

    private static final String API_KEY="0bd118e9f2d56bce1854ac53c891a2b8";
    private RecyclerView mRecyclerView;
    private Context mContext;
    MovieAdapter mAdapter;
    private DividerItemDecoration mDividerItemDecoration;

    TextView mEmptyTextView;
    ProgressBar mProgressBar;
    TextView mCriteria;

    String certification;
    String fromYear;
    String toYear;
    String genre;
    String type;
    private String sort;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView= (RecyclerView) findViewById(recyclerView);
        mEmptyTextView = (TextView) findViewById(R.id.textView_emptyList);
        mProgressBar= (ProgressBar) findViewById(R.id.progressBar);
        mCriteria= (TextView) findViewById(R.id.textView_criteria);
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);

        mContext=this;



      /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);

        mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        mDividerItemDecoration.setDrawable(mContext.getResources().getDrawable(R.drawable.dividercolor));

        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        Intent extras=getIntent();
        certification=extras.getStringExtra("certification");
        fromYear=extras.getStringExtra("fromYear");
        toYear=extras.getStringExtra("toYear");
        genre=extras.getStringExtra("genre");
        type=extras.getStringExtra("type");
        sort=extras.getStringExtra("sort");

        mCriteria.setText("Genre: "+ genre +" | Year: "+fromYear+" - "+toYear+" | Type: "+type+" | Rating: "+certification+"");

        mProgressBar.setVisibility(View.VISIBLE);

        if (genre.equals("Any")){
            genre=null;

        }else {
            genre= UtilityFunctions.genreChooser(genre);
        }

        if (certification.equals("Any")){
            certification=null;
        }

        if (type.equals("Any") || type.equals("Movie")){
            Call<MovieResponse> call= apiService.getMovieDetails(API_KEY,certification,fromYear,toYear,genre,sort);
            call.enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                    final ArrayList<Movie> movies=response.body().getMovies();

                    Log.d(TAG,"Number of movies received: " +movies.size());
                    if (response.isSuccessful()){
                        if (movies.isEmpty()) {mEmptyTextView.setVisibility(View.VISIBLE);mEmptyTextView.setText("No Result Found"); }
                        mProgressBar.setVisibility(View.INVISIBLE);
                        //  mEmptyTextView.setVisibility(View.INVISIBLE);
                        mAdapter=new MovieAdapter(movies,mContext);
                        mRecyclerView.setAdapter(mAdapter);
                    }

                      mAdapter.SetOnItemClickListener(new MovieAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            ReferenceMap.getInstance().putObjectReference(DetailActivity.class, "myKey", movies.get(position));


                            Intent intent=new Intent(ResultActivity.this,DetailActivity.class);
                            startActivity(intent);
                            //  String origianlname= movies.get(position).getOriginalTitle()
                            //  Log.d(TAG,"Number of movies received: " +origianlname);
                        }
                    });

                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable t) {
                    mEmptyTextView.setVisibility(View.VISIBLE);
                    mEmptyTextView.setText("No Result Found");
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            });


        }

        if (type.equals("TV")){
            Call<MovieResponse> call= apiService.getTVDetails(API_KEY,certification,fromYear,toYear,genre,sort);
            call.enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                    final ArrayList<Movie> movies=response.body().getMovies();

                    Log.d(TAG,"Number of TV received: " +movies.size());
                    if (response.isSuccessful()){
                        if (movies.isEmpty()) {mEmptyTextView.setVisibility(View.VISIBLE);mEmptyTextView.setText("No Result Found"); }
                        mProgressBar.setVisibility(View.INVISIBLE);
                        //  mEmptyTextView.setVisibility(View.INVISIBLE);
                        mAdapter=new MovieAdapter(movies,mContext);
                        mRecyclerView.setAdapter(mAdapter);

                    }


                    mAdapter.SetOnItemClickListener(new MovieAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            ReferenceMap.getInstance().putObjectReference(DetailActivity.class, "myKey", movies.get(position));


                            Intent intent=new Intent(ResultActivity.this,DetailActivity.class);
                            startActivity(intent);
                            //  String origianlname= movies.get(position).getOriginalTitle()
                            //  Log.d(TAG,"Number of movies received: " +origianlname);
                        }
                    });

                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable t) {
                    mEmptyTextView.setVisibility(View.VISIBLE);
                    mEmptyTextView.setText("No Result Found");
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            });


        }



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

     //   item.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        if (id == R.id.nav_search) {
            Intent intent=new Intent(ResultActivity.this,MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_watch) {
            Intent intent=new Intent(ResultActivity.this,MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            share();
        } else if (id == R.id.nav_rate) {
            rateMe();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void share(){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,"Hey, \n" +
                "Anime Movie Recommender app is available on Google Play. \n" +
                "https://play.google.com/store/apps/details?id=android.animemovierecommender.apkia.com.animemovierecommender");

        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_chooser_title)));

    }


    void rateMe(){
        Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + mContext.getPackageName())));
        }
    }
}
