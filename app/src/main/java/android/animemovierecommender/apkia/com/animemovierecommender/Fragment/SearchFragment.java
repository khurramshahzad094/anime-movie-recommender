package android.animemovierecommender.apkia.com.animemovierecommender.Fragment;


import android.animemovierecommender.apkia.com.animemovierecommender.Activity.ResultActivity;
import android.animemovierecommender.apkia.com.animemovierecommender.R;
import android.animemovierecommender.apkia.com.animemovierecommender.helper.UtilityFunctions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {


    private String toyear;
    private String genre;
    private String rating;
    private String type;
    private String fromyear;

    //Activity context;
    private String sort="popularity.desc";
    private Context mContext;

    public SearchFragment() {
        // Required empty public constructor
    }

    EditText titleEdit;
    Spinner genreSpinner;
    Spinner typeSpinner;
    Spinner ratingSpinner;
    Spinner toYearSpinner;
    Spinner fromYearSpinner;
    Button searchButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_search, container, false);

        mContext=getActivity();

        genreSpinner= (Spinner) view.findViewById(R.id.spinner_genre);
        typeSpinner= (Spinner) view.findViewById(R.id.spinner_type);
        ratingSpinner= (Spinner) view.findViewById(R.id.spinner_rating);
        toYearSpinner= (Spinner) view.findViewById(R.id.spinner_yearto);
        fromYearSpinner= (Spinner) view.findViewById(R.id.spinner_yearfrom);
        searchButton= (Button) view.findViewById(R.id.button_search);

        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> genreList=ArrayAdapter.createFromResource(
                getActivity(),R.array.genre_array,android.R.layout.simple_spinner_item);


        // Specify the layout to use when the list of choices appears
        genreList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        genreSpinner.setAdapter(genreList);

        genreSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genre=(String) parent.getItemAtPosition(position);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

               // genre=UtilityFunctions.genreChooser(genre);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> typeList=ArrayAdapter.createFromResource(
                getActivity(),R.array.type_array,android.R.layout.simple_spinner_item);


        // Specify the layout to use when the list of choices appears
        typeList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        typeSpinner.setAdapter(typeList);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type=(String) parent.getItemAtPosition(position);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> ratingList=ArrayAdapter.createFromResource(
                getActivity(),R.array.rating_array,android.R.layout.simple_spinner_item);


        // Specify the layout to use when the list of choices appears
        ratingList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        ratingSpinner.setAdapter(ratingList);

        ratingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rating=(String) parent.getItemAtPosition(position);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1900; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> yearsList = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, years);
        toYearSpinner.setAdapter(yearsList);
        fromYearSpinner.setAdapter(yearsList);


        toYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                toyear=(String) parent.getItemAtPosition(position);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        fromYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fromyear=(String) parent.getItemAtPosition(position);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (UtilityFunctions.isNetworkAvailable(mContext)){
               Intent intent=new Intent(mContext, ResultActivity.class);
                intent.putExtra("genre",genre);
                intent.putExtra("type",type);
                intent.putExtra("certification",rating);
                intent.putExtra("fromYear",fromyear);
                intent.putExtra("toYear",toyear);
                intent.putExtra("sort",sort);
                startActivity(intent);
                }
                else {
                    UtilityFunctions.alertUserAboutError(mContext);
                }
            }
        });


        return view;
    }








}
